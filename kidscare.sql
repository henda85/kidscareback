CREATE DATABASE kidscare
  DEFAULT CHARACTER SET = 'utf8mb4';


DROP TABLE IF EXISTS `kids`;
CREATE TABLE `kids` (
  `kidId` INT(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `kidName` VARCHAR(255) DEFAULT NULL,
  `userId` INT(20) DEFAULT NULL,
  `classId` INT(20) DEFAULT NULL,
  PRIMARY KEY (`kidId`),
  KEY `userId_fk` (`userId`),
  KEY `classId_fk` (`classId`),
  CONSTRAINT `classId_fk` FOREIGN KEY (`classId`) REFERENCES `ClassRoom` (`classId`),
  CONSTRAINT `UserId_fk` FOREIGN KEY (`userId`) REFERENCES `User` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `userId` INT(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `passWord` VARCHAR(255) NOT NULL,
  `userName` VARCHAR(255) NOT NULL,
  `userEmail` VARCHAR(255) NOT NULL,
  `userAdress` VARCHAR(255) NOT NULL,
  `userPhone` VARCHAR(255) NOT NULL,
  `userAvtar` VARCHAR(255) DEFAULT NULL,
  `userDisponibility` int NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
DROP TABLE IF EXISTS `Messages`;
CREATE TABLE `Messages` (
  `messageId` INT(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `dateTime` date NOT NULL DEFAULT (curdate()),
  `userMessId` INT(20) NOT NULL,
  `text` LONGTEXT NOT NULL,
  PRIMARY KEY (`messageId`),
  KEY `userMessId_fk` (`userMessId`),
  CONSTRAINT `userMessId_fk` FOREIGN KEY (`userMessId`) REFERENCES `User` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;


CREATE TABLE `ClassRoom` (
  `classId` INT(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `classLevel` VARCHAR(255) NOT NULL,
  `schoolId` mediumtext NOT NULL,
  PRIMARY KEY (`classId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;