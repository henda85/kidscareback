import { User as user } from "./src/entity/user";

declare global {
  namespace Express {
    interface User extends user {}
  }
}
