import express from "express";
import cors from "cors";
import { userController } from "./controller/ user-controller";
import { messageController } from "./controller/message-controller";
import { configurePassport } from "./utils/token";
import passport from "passport";
import { kidController } from "./controller/kid-controller";
//import { classRoomController } from "./controller/classRoom-controller";

export const server = express();

configurePassport();

server.use(passport.initialize());
server.use(express.json());
server.use(cors());
server.use("/api/user", userController);
server.use("/api/message", messageController);
server.use("/api/kid", kidController);

//server.use("/api/classRoom", classRoomController);
