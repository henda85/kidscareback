import { Router } from "express";
import { User, schema } from "../entity/user";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from "bcrypt";
import { generateToken } from "../utils/token";
import passport from "passport";

export const userController = Router();
//register route

userController.post("/", async (req, res) => {
  try {
    const { error } = schema.validate(req.body, {
      abortEarly: false,
      allowUnknown: true,
    });
    if (error) {
      res
        .status(400)
        .json({ error: error.details.map((item) => item.message) });
      return;
    }
    const newUser = new User();
    Object.assign(newUser, req.body);
    const exists = await UserRepository.findByEmail(newUser.userEmail);
    if (exists) {
      res.status(400).json({ error: "Email already taken" });
      return;
    }
    newUser.passWord = await bcrypt.hash(newUser.passWord, 11);
    await UserRepository.add(newUser);
    res.status(201).json({
      user: newUser,
      token: generateToken({
        email: newUser.userEmail,
        id: newUser.userId,
      }),
    });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

userController.post("/login", async (req, res) => {
  try {
    const user = await UserRepository.findByEmail(req.body.email);
    if (user) {
      const samePassword = await bcrypt.compare(
        req.body.password,
        user.passWord
      );
      if (samePassword) {
        res.json({
          user,
          token: generateToken({
            email: user.userEmail,
            id: user.userId,
          }),
        });
        return;
      }
    }
    res.status(401).json({ error: "Wrong email and/or password" });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});

/**
 * On indique à passport que cette route est protégée par un JWT, si on tente d'y
 * accéder sans JWT dans le header ou avec un jwt invalide, l'accès sera refusé
 * et express n'exécutera pas le contenu de la route
 */
userController.get(
  "/account",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    //comme on est dans une route protégée, on peut accéder à l'instance de User correspondant
    //au token avec req.user
    res.json(req.user);
  }
);
