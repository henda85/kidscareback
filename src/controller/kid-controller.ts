import { Router } from "express";
import passport from "passport";
import { ClassRoom } from "../entity/classRoom";
import { ClassRoomRepository } from "../repository/ClassRoomRepository";
import { KidsRepository } from "../repository/KidsRepository";

export const kidController = Router();

kidController.get("/:id", async (req, res) => {
  try {
    const kids = await KidsRepository.findKidsByUser(Number(req.params.id));
    res.json(kids);
    res.end();
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "error error" });
  }
});
kidController.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let classRoom = new ClassRoom();
    classRoom = await ClassRoomRepository.findClassRoom(
      req.body.classLevel,
      req.body.schoolId
    );
    res.end();
    if (classRoom) {
      await KidsRepository.add(
        req.body.kidName,
        req.user.userId,
        classRoom.classId
      );
      res.end();
    } else {
      classRoom = new ClassRoom(req.body.classLevel, req.body.schoolId);
      await ClassRoomRepository.add(classRoom);
      res.end();
      await KidsRepository.add(
        req.body.kidName,
        req.user.userId,
        classRoom.classId
      );
      res.end();
    }
  }
);
//a ajouter si j'ai le temps deletekids et updatekids
