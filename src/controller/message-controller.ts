import { Router } from "express";
import passport from "passport";
import { Messages } from "../entity/messages";
import { MessageRepository } from "../repository/MessageRepository";

export const messageController = Router();

messageController.get("/", async (req, res) => {
  try {
    const messages = await MessageRepository.findAllMessages();
    res.json(messages);
    res.end();
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "error error" });
  }
});
messageController.post(
  "/",
  passport.authenticate("jwt", { session: false }),

  async (req, res) => {
    let message = new Messages();
    Object.assign(message, req.body);

    try {
      await MessageRepository.add(message, req.user.userId);
      res.end();
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error error" });
    }
  }
);

messageController.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const messages = await MessageRepository.findMessageByUser(
      Number(req.user.userId)
    );

    let exist = false;
    for (let row of messages) {
      if (Number(req.params.id) === row.messageId) {
        exist = true;
      }
    }
    if (exist) {
      try {
        await MessageRepository.delete(Number(req.params.id));
        res.end();
      } catch (error) {
        console.log(error);
        res.status(500).json({ message: "error error" });
      }
    } else {
      console.log("you are not the author of this message ");
    }
  }
);
messageController.put("/:id", async (req, res) => {
  await MessageRepository.update(req.body, Number(req.params.id));
  res.end();
});
