import { ResultSetHeader, RowDataPacket } from "mysql2";
import { ClassRoom } from "../entity/classRoom";
import { connection } from "./connection";

export class ClassRoomRepository {
  static async add(classRoom: ClassRoom) {
    const [rows] = await connection.query<ResultSetHeader>(
      "INSERT INTO ClassRoom (classLevel,schoolId) VALUES (?,?)",
      [classRoom.classLevel, classRoom.schoolId]
    );
    classRoom.classId = rows.insertId;
  }
  static async findClassRoom(classLevel: string, schoolId: string) {
    const [rows] = await connection.query<RowDataPacket[]>(
      "SELECT * FROM ClassRoom WHERE classLevel =? AND schoolId=?",
      [classLevel, schoolId]
    );
    if (rows.length === 1) {
      return new ClassRoom(
        rows[0].classLevel,
        rows[0].classId,
        rows[0].schoolId
      );
    }
    return null;
  }
  static async findAllClassbySchool(schoolId: string) {
    let [rows] = await connection.query<RowDataPacket[]>(
      "SELECT * FROM ClassRoom WHERE schoolId =?",
      [schoolId]
    );
    let classRooms: ClassRoom[] = [];
    for (const row of rows) {
      let instance = new ClassRoom(row.kidName, row.kidId);
      classRooms.push(instance);
    }
    return classRooms;
  }
  static async findSchoolId(classId: number) {
    let [rows] = await connection.query<RowDataPacket[]>(
      "SELECT schoolId FROM ClassRoom WHERE classId =?",
      [classId]
    );
    return rows[0].schoolId;
  }
  static async findClassLevel(classId: number) {
    let [rows] = await connection.query<RowDataPacket[]>(
      "SELECT classLevel FROM ClassRoom WHERE classId =?",
      [classId]
    );
    return rows[0].classLevel;
  }
}
