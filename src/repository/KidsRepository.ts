import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Kids } from "../entity/kids";
import { ClassRoomRepository } from "./ClassRoomRepository";
import { connection } from "./connection";

export class KidsRepository {
  static async add(kidName: string, Iduser: number, classId: number) {
    await connection.query<ResultSetHeader>(
      "INSERT INTO kids (kidName,userId,classId) VALUES (?,?,?)",
      [kidName, Iduser, classId]
    );
  }
  static async findKidsByUser(Id: number) {
    const [rows] = await connection.query<RowDataPacket[]>(
      `SELECT DISTINCT kids.kidId,ClassRoom.classId,kids.kidName,
      ClassRoom.classLevel,ClassRoom.schoolId FROM kids,ClassRoom WHERE kids.userId=?
        AND kids.classId=ClassRoom.classId;`,
      [Id]
    );
    let kids: Kids[] = [];
    for (const row of rows) {
      let instance = new Kids(
        row.kidName,
        row.kidId,
        row.classId,
        row.schoolId,
        row.classLevel
      );
      kids.push(instance);
    }
    return kids;
  }
}
