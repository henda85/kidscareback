import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Kids } from "../entity/kids";
import { User } from "../entity/user";
import { ClassRoomRepository } from "./ClassRoomRepository";
import { connection } from "./connection";
import { KidsRepository } from "./KidsRepository";

export class UserRepository {
  static async add(user: User) {
    const [rows] = await connection.query<ResultSetHeader>(
      "INSERT INTO User (passWord,userName,userEmail,userAdress,userPhone,userDisponibility,userAvtar) VALUES (?,?,?,?,?,?,?)",
      [
        user.passWord,
        user.userName,
        user.userEmail,
        user.userAdress,
        user.userPhone,
        user.userDisponibility,
        user.userAvtar,
      ]
    );
    user.userId = rows.insertId;
  }
  static async findByEmail(email: string) {
    const [rows] = await connection.query<RowDataPacket[]>(
      "SELECT * FROM User WHERE userEmail=?",
      [email]
    );
    if (rows.length === 1) {
      const user = new User(
        rows[0].passWord,
        rows[0].userName,
        rows[0].userEmail,
        rows[0].userAdress,
        rows[0].userPhone,
        rows[0].userDisponibility,
        rows[0].userAvtar,
        rows[0].userId
      );
      user.kids = await KidsRepository.findKidsByUser(user.userId);
      return user;
    }
    return null;
  }
}
