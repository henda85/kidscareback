import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Messages } from "../entity/messages";
import { connection } from "./connection";

export class MessageRepository {
  static async add(message: Messages, Id: number) {
    const [rows] = await connection.query<ResultSetHeader>(
      "INSERT INTO Messages (text,datetime,userMessId) VALUES (?,NOW(),?)",
      [message.text, Id]
    );
    message.messageId = rows.insertId;
  }
  static async findAllMessages() {
    const [rows] = await connection.query<RowDataPacket[]>(
      "SELECT messageId,text, datetime, userName, userAvtar from Messages LEFT JOIN User ON User.userId = Messages.userMessId ORDER BY datetime"
    );
    let messages: Messages[] = [];
    for (const row of rows) {
      let instance = new Messages(
        row.text,
        row.datetime,
        row.messageId,
        row.userName,
        row.userAvtar
      );
      messages.push(instance);
    }
    return messages;
  }
  static async update(message: Messages, Id: number) {
    await connection.query(
      `UPDATE Messages SET text=?, dateTime=NOW() WHERE messageId=?`,
      [message.text, Id]
    );
  }
  static async delete(id: number) {
    await connection.query(`DELETE FROM Messages WHERE messageId=?`, [id]);
  }
  static async findMessageByUser(Id: number) {
    const [rows] = await connection.query<RowDataPacket[]>(
      `SELECT * FROM  Messages WHERE userMessId=?`,
      [Id]
    );
    let messages: Messages[] = [];
    for (const row of rows) {
      let instance = new Messages(
        row.text,
        row.datetime,
        row.messageId,
        row.userName,
        row.userAvtar
      );
      messages.push(instance);
    }
    return messages;
  }
}
