export class Kids {
  constructor(
    public kidName: string = null,
    public kidId: number = null,
    public classId: number = null,
    public schoolId: String = null,
    public classLevel: String = null
  ) {}
}
