import Joi from "joi";
import { Kids } from "./kids";

export class User {
  constructor(
    public passWord: string = null,
    public userName: string = "0",
    public userEmail: string = null,
    public userAdress: string = null,
    public userPhone: string = null,
    public userDisponibility: number = 0,
    public userAvtar: string = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
    public userId: number = null,
    public kids: Kids[] = []
  ) {}

  toJSON() {
    return {
      userId: this.userId,
      userName: this.userName,
      userEmail: this.userEmail,
      userAdress: this.userAdress,
      userPhone: this.userPhone,
      userDisponibility: this.userDisponibility,
      userAvtar: this.userAvtar,
      kids: this.kids,
    };
  }
}

export const schema = Joi.object({
  userEmail: Joi.string().email().required(),
  passWord: Joi.string().min(4).required(),
  userPhone: Joi.string().required(),
});
