export class ClassRoom {
  constructor(
    public classLevel: string = null,
    public schoolId: string = null,
    public classId: number = null
  ) {}
}
