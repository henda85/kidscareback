import Joi from "joi";

export class Messages {
  constructor(
    public text: string = null,
    public datetime: Date = null,
    public messageId: number = null,
    public auteur: string = null,
    public avatar: string = null
  ) {}
}
// export const schema = Joi.object({
//   datetime: Joi.date(),
//   text: Joi.string(),
// });
